---
title: Stateless OpenPGP Command Line Interface
docname: draft-dkg-openpgp-stateless-cli-01
date: 2019-10-28
category: info

ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    street: 125 Broad St.
    city: New York, NY
    code: 10004
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
informative:
 I-D.draft-bre-openpgp-samples-00:
 OpenPGP-Interoperability-Test-Suite:
    target:  https://tests.sequoia-pgp.org/
    title: OpenPGP Interoperability Test Suite
    date: 2019-10-28
normative:
 RFC2119:
 RFC8174:
 RFC4880:
 I-D.ietf-openpgp-rfc4880bis:
--- abstract

This document defines a generic stateless command-line interface for dealing with OpenPGP messages, known as `sop`.
It aims for a minimal, well-structured API for dealing with OpenPGP object security.

--- middle

Introduction
============

Different OpenPGP implementations have many different requirements, which typically break down in two main categories: key/certificate management and object security.

The purpose of this document is to provide a "stateless" interface that primarily handles the object security side of things, and assumes that secret key management and certificate management will be handled some other way.

This separation should make it easier to provide interoperability testing for the object security work, and to allow implementations to consume and produce new cryptographic primitives as needed.

This document defines a generic stateless command-line interface for
dealing with OpenPGP messages, known here by the placeholder `sop`.  It aims for a minimal,
well-structured API.

An OpenPGP implementation should not name its executable `sop` to implement this specification, of course.  It just needs to provide a binary that conforms to this interface.

A `sop` implementation should leave no trace on the system, and its behavior should not be affected by anything other than command-line arguments and input.

Obviously, the user will need to manage their secret keys (and their
peers' certificates) somehow, but the goal of this interface is to
separate out that task from the task of interacting with OpenPGP
messages.

While this document identifies a command-line interface, the rough
outlines of this interface should also be amenable to relatively
straightforward library implementations in different languages.

Requirements Language
---------------------

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in BCP 14 {{RFC2119}} {{RFC8174}} when, and only when, they appear in all capitals, as shown here.

Terminology
-----------

This document uses the term "key" to refer exclusively to OpenPGP Transferable Secret Keys (see section 11.2 of {{RFC4880}}).

It uses the term "certificate" to refer to OpenPGP Transferable Public Key (see section 11.1 of {{RFC4880}}).

"Stateless" in "Stateless OpenPGP" means avoiding secret key and certificate state.
The user is responsible for managing all OpenPGP certificates and secret keys themselves, and
passing them to `sop` as needed.
The user should also not be concerned that any state could affect the underlying operations.

Examples
========

These examples show no error checking, but give a flavor of how `sop` might be used in practice from a shell.

The key and certificate files described in them (e.g. `alice.sec`) could be for example those found in {{I-D.draft-bre-openpgp-samples-00}}.

~~~
sop generate "Alice Lovelace <alice@openpgp.example>" > alice.sec
sop convert < alice.sec > alice.pgp

sop sign --as=text alice.sec < announcement.txt > announcement.txt.asc
sop verify announcement.txt.asc alice.pgp < announcement.txt

sop encrypt --sign-with=alice.sec --as=mime bob.pgp < msg.eml > encrypted.asc
sop decrypt alice.sec < ciphertext.asc > cleartext.out
~~~

Subcommands
===========

`sop` uses a subcommand interface, similar to those popularized by systems like `git` and `svn`.

If the user supplies a subcommand that `sop` does not implement, it
fails with a return code of 69.  If a `sop` implementation does not
handle a supplied option for a given subcommand, it fails with a
return code of 37.

For all commands that have an `--armor|--no-armor` option, it defaults
to `--armor`, meaning that any output OpenPGP material should be
ASCII-armored (section 6 of {{I-D.ietf-openpgp-rfc4880bis}})
by default.

Version Information
-------------------

    sop version

 - Standard Input: ignored
 - Standard Output: version string

The version string emitted should contain the name of the `sop`
implementation, followed by a single space, followed by the version
number.


Generate a Secret Key
---------------------

    sop generate [--armor|--no-armor] [--] [USERID…]

 - Standard Input: ignored
 - Standard Output: `KEY` ({{key}})

Generate a single default OpenPGP certificate with zero or more User
IDs.

Convert a Secret Key to a Certificate
-------------------------------------

    sop convert [--armor|--no-armor]

 - Standard Input: `KEY` ({{key}})
 - Standard Output: `CERT` ({{cert}})


Create a Detached Signature
---------------------------

    sop sign [--armor|--no-armor]
         [--as={binary|text}] [--] KEY [KEY...]

 - Standard Input: `DATA` ({{data}})
 - Standard Output: `SIGNATURE` ({{signature}})

`--as` defaults to `binary`.  If `--as=text` and the input `DATA` is
not valid `UTF-8`, `sop sign` fails with a return code of 53.

Verify a Detached Signature
---------------------------

    sop verify [--not-before=DATE] [--not-after=DATE]
        [--] SIGNATURE CERT [CERT...]

 - Standard Input: `DATA` ({{data}})
 - Standard Output: `VERIFICATIONS` ({{verifications}})

`--not-before` and `--not-after` indicate that only signatures with
dates in a certain range should be considered as possibly valid.

`--not-before` defaults to the beginning of time.

`--not-after` defaults to "now".

`sop verify` only returns 0 if at least one of the supplied `CERT`s
made a valid signature in the range over the `DATA` supplied.

For details about the valid signatures, the user MUST inspect the
`VERIFICATIONS` output.

If no `CERT` is supplied, `sop verify` fails with a return code of 19.

If at least one `CERT` is supplied, but no valid signatures are found,
`sop verify` fails with a return code of 3.

See {{signature-verification}} for more details about signature verification.

Encrypt a Message
-----------------

    sop encrypt [--as={binary|text|mime}]
        [--armor|--no-armor]
        [--mode={any|communications|storage}]
        [--with-password=PASSWORD...]
        [--session-key=SESSIONKEY]
        [--sign-with=KEY...]
        [--] [CERT...]

 - Standard Input: `DATA` ({{data}})
 - Standard Output: `CIPHERTEXT` ({{ciphertext}})

`--as` defaults to `binary`.

`--mode` defaults to `any`, meaning any encryption-capable subkey may be used.

`--with-password` enables symmetric encryption (and can be used
multiple times if multiple passwords are desired).  If `sop encrypt`
encounters a `PASSWORD` which is not a valid `UTF-8` string, it fails
with a return code of 31.  If `sop encrypt` sees trailing whitespace
at the end of a `PASSWORD`, it will trim the trailing whitespace
before using the password.

`--session-key` permits the encryptor to select the symmetric
encryption algorithm and specific session key.

`--sign-with` enables signing by a secret key (and can be used
multiple times if multiple signatures are desired).

If `--as` is set to either `--text` or `--mime`, then `--sign-with`
will sign as a canonical text document.  In this case, if the input
`DATA` is not valid `UTF-8`, `sop encrypt` fails with a return code of
53.

The resulting `CIPHERTEXT` should be decryptable by the secret
keys corresponding to each identified `CERT`.

If no `CERT` or `--with-password` options are present, `sop encrypt`
fails with a return code of 19.

Decrypt a Message
-----------------

    sop decrypt [--session-key-out=SESSIONKEY]
        [--with-password=PASSWORD...]
        [--verify-out=VERIFICATIONS
         [--verify-with=CERT...]
         [--verify-not-before=DATE]
         [--verify-not-after=DATE] ]
        [--] [KEY...]

 - Standard Input: `CIPHERTEXT` ({{ciphertext}})
 - Standard Output: `DATA` ({{data}})

`--session-key-out` can be used to learn the session key on
successful decryption.

If `sop decrypt` fails for any reason and the identified `SESSIONKEY`
file already exists in the filesystem, the file will be unlinked.

`--with-password` enables symmetric decryption (and can be used
multiple times if the user wants to try more password are tried).

If `sop decrypt` tries and fails to use a supplied `PASSWORD`, and it
observes that there is trailing `UTF-8` whitespace at the end of the
`PASSWORD`, it will retry with the trailing whitespace stripped.

`--verify-out` produces signature verification status to the
designated file.

`sop decrypt` does not fail (that is, the return code is not modified)
based on the results of signature verification.  The caller MUST check
the returned `VERIFICATIONS` to confirm signature status.  An empty
`VERIFICATIONS` output indicates that no valid signatures were found.
If `sop decrypt` itself fails for any reason, and the identified
`VERIFICATIONS` file already exists in the filesystem, the file will
be unlinked.

`--verify-with` identifies a certificate whose signatures would be
acceptable for signatures over this message.

If the caller is interested in signature verification, both
`--verify-out` and at least one `--verify-with` must be supplied.  If
only one of these arguments is supplied, `sop decrypt` fails with a
return code of 23.

`--verify-not-before` and `--verify-not-after` provide a date range
for acceptable signatures, by analogy with the options for `sop
verify`.  They should only be supplied when doing signature
verification.

See {{signature-verification}} for more details about signature verification.

If no `KEY` or `--with-password` options are present, `sop decrypt`
fails with a return code of 19.

If unable to decrypt, `sop decrypt` fails with a return code of 29.

`sop decrypt` only returns cleartext to Standard Output that was
successfully decrypted.

Adding ASCII Armor
------------------

    sop armor [--label={sig|key|cert|message}]

 - Standard Input: 8-bit, unarmored OpenPGP material (`SIGNATURE`, `CERT`, `KEY`, or `CIPHERTEXT`)
 - Standard Output: the same material with ASCII-armoring added

The user can choose to specify the label used in the header and tail
of the armoring.  If the user does not specify, `sop` inspects the
input and chooses the label appropriately.  If `sop` cannot select a
label on the basis of the input, it treats it as literal data, and
labels it as a `message`.

Removing ASCII Armor
--------------------

    sop dearmor

 - Standard Input: ASCII-armored OpenPGP material (`CIPHERTEXT`, `SIGNATURE`, `CERT`, or `KEY`)
 - Standard Output: the same material with ASCII-armoring removed

Input/Output Indirect Types
===========================

Some material is passed to `sop` indirectly, typically by referring to
a filename containing the data in question.  This type of data may
also be passed to `sop` on Standard Input, or delivered by `sop` to
Standard Output.

If the filename for any indirect material used as input has the
special form `@ENV:xxx`, then contents of environment variable `$xxx`
is used instead of looking in the filesystem.

If the filename for any indirect material used as either input or
output has the special form `@FD:nnn` where `nnn` is a decimal
integer, then the associated data is read from file descriptor `nnn`.

If any input data does not meet the requirements described below,
`sop` will fail with a return code of 41.

CERT {#cert}
----

One OpenPGP certificate (section 11.1 of {{I-D.ietf-openpgp-rfc4880bis}})
aka "Transferable Public Key".
May be armored.

KEY {#key}
-----

One OpenPGP Transferable Secret Key (section 11.2 of {{I-D.ietf-openpgp-rfc4880bis}}).
May be armored.

Secret key material should be in cleartext (that is, it should not be
locked with a password).  If the secret key maerial is locked with a
password, `sop` may fail to use the key.

CIPHERTEXT {#ciphertext}
----------

`sop` accepts only a restricted subset of the arbitrarily-nested
grammar allowed by the OpenPGP Messages
definition (section 11.3 of {{I-D.ietf-openpgp-rfc4880bis}}).

In particular, it accepts and generates only:

An OpenPGP message, consisting of a sequence of
PKESKs (section 5.1 of {{I-D.ietf-openpgp-rfc4880bis}})
and
SKESKs (section 5.3 of {{I-D.ietf-openpgp-rfc4880bis}}),
followed by one
SEIPD (section 5.14 of {{I-D.ietf-openpgp-rfc4880bis}}).

The SEIPD can decrypt into one of two things:

  - "Maybe Signed Data" (see below), or

  - Compressed data packet that contains "Maybe Signed Data"

"Maybe Signed Data" is a sequence of:

  - N (zero or more) one-pass signature packets, followed by
  - zero or more signature packets, followed by
  - one Literal data packet, followed by
  - N signature packets (corresponding to the outer one-pass signatures packets)

FIXME: does any tool do compression inside signing?  Do we need to
handle that?

May be armored.

SIGNATURE {#signature}
---------
One or more OpenPGP Signature packets.  May be armored.

SESSIONKEY {#sessionkey}
----------

This documentation uses the GnuPG defacto `ASCII` representation:

`ALGONUM:HEXKEY`

where `ALGONUM` is the decimal value associated with the OpenPGP
Symmetric Key Algorithms (section 9.3 of
{{I-D.ietf-openpgp-rfc4880bis}}).

As input, `ALGONUM:` alone (with an empty `HEXKEY`) means "user
specifies the algorithm, but the implementation chooses an arbitrary
key for the cipher."

Example AES-256 session key:

    9:FCA4BEAF687F48059CACC14FB019125CD57392BAB7037C707835925CBF9F7BCD

PASSWORD {#password}
--------

This is expected to be a `UTF-8` string, but for `sop decrypt`, any
bytestring that the user supplies will be accepted.  Note the details
in `sop encrypt` and `sop decrypt` about trailing whitespace!

VERIFICATIONS {#verifications}
-------------

One line per successful signature verification.  Each line has two
structured fields delimited by a single space, followed by arbitary
text to the end of the line.

 - ISO-8601 UTC datestamp
 - Fingerprint of primary key of signing certificate
 - arbitrary text

Example:

    2019-10-24T23:48:29Z C4BC2DDB38CCE96485EBE9C2F20691179038E5C6 signed by dkg!

DATA {#data}
----

Cleartext, arbitrary data.  This is either a bytestream or `UTF-8`
text.

It MUST only be `UTF-8` text in the case of input supplied to `sop sign --as=text` or `sop encrypt --as={mime|text}`.
If `sop` receives `DATA` containing non-`UTF-8` octets in this case, it will fail with return code 53.


Failure modes
=============

When `sop` succeeds, it will return 0 and emit nothing to Standard
Error.  When `sop` fails, it fails with a non-zero return code, and
emits one or more warning messages on Standard Error.  Known return
codes include:

Return | Meaning
---:|--------------------------------------------------
 0 | Success
 3 | No acceptable signatures found (`sop verify`)
19 | Missing required argument
23 | Incomplete verification instructions (`sop decrypt`)
29 | Unable to decrypt (`sop decrypt`)
31 | Non-`UTF-8` password (`sop encrypt`)
37 | Unsupported option
41 | Invalid data type (no secret key where `KEY` expected, etc)
53 | Non-text input where text expected
69 | Unsupported subcommand

A `sop` implementation MAY return other error codes than those listed
above.


Guidance for Implementors
=========================

`sop` uses a few assumptions that implementers might want to consider.

One OpenPGP Message At a Time
-----------------------------

`sop` is intended to be a simple tool that operates on one OpenPGP object at a time.  It should be composable, if you want to use it to deal with multiple OpenPGP objects

FIXME: discuss what this means for streaming.
The stdio interface doesn't necessarily imply streamed output.

Simplified Subset of OpenPGP Message
------------------------------------

While the formal grammar for OpenPGP Message is arbitrarily nestable,`sop` constrains itself to what it sees as a single "layer" (see {{ciphertext}}).

This is a deliberate choice, because it is what most consumers expect, and runaway recursion is bad news.

Note that an implementation of `sop decrypt` MAY choose to handle more complex structures, but if it does, it should document the other structures it handles and why it chooses to do so.
We can use such documentation to improve future versions of this spec.

Validate Signatures Only From Known Signers
-------------------------------------------

There are generally only a few signers who are relevant for a given OpenPGP message.
When verifying signatures, `sop` expects that the caller can identify those relevant signers ahead of time.

Detached Signatures {#detached-signatures}
-------------------

`sop` deals with detached signatures as the baseline form of OpenPGP signatures.

The main problem this avoids is the trickiness of handling a signature that is mixed inline into the data that it is signing.

Reliance on Supplied Certs and Keys {#cert-validity-performance}
-----------------------------------

A truly stateless implementation may find that it spends more time validating the internal consistency of certificates and keys than it does on the actual object security operations.

For performance reasons, an implementation may choose to ignore validation on certificate and key material supplied to it.  The security implications are of doing so depend on how the certs and keys are managed outside of `sop`.

Guidance for Consumers
======================

While `sop` is originally conceived of as an interface for interoperability testing, it's conceivable that an application that uses OpenPGP for object security would want to use it.

FIXME: more guidance for how to use such a tool safely and efficiently goes here.

FIXME: if an encrypted OpenPGP message arrives without metadata, it is difficult to know which signers to consider when decrypting.
How do we do this efficiently without invoking `sop decrypt` twice, once without `--verify-*` and again with the expected identity material?

Security Considerations
=======================

The OpenPGP object security model is typically used for confidentiality and authenticity purposes.

Signature Verification {#signature-verification}
----------------------

In many contexts, an OpenPGP signature is verified, to prove the origin and integrity of an underlying object.

When `sop` checks a signature (e.g. via `sop verify` or `sop decrypt --verify-with`, it should only consider it to be verified only if at least all of these conditions are met:

 * The signature must be made by a signing-capable public key that is present in one of the supplied `CERT`s
 * The `CERT` and signing subkey must have been created before or at the signature time
 * The `CERT` and signing subkey must not have been expired at the signature time
 * The `CERT` and signing subkey must not be revoked with a "hard" revocation
 * If the `CERT` or signing subkey is revoked with a "soft" revocation, then the signature time must predate the revocation
 * The signing subkey must be properly bound to the primary key, and cross-signed
 * The signature (and any dependent signature, such as the cross-sig or subkey binding signatures) must be made with strong cryptographic algorithms (e.g., not `MD5` or a 1024-bit `RSA` key)

Implementers should typically also consider other factors in addition to the origin and authenticity, including application-specific information.

For example, consider the application domain of checking software updates.

If software package Foo version 13.3.2 was signed on 2019-10-04, and the user receives a copy of Foo version 12.4.8 that was signed on 2019-10-16, it may be authentic and have a more recent signature date.
But it is not an upgrade (12.4.8 < 13.3.2), and therefore it should not be applied automatically.

In such cases, it is critical that the application confirms that the other information verified is *also* protected by the relevant OpenPGP signature.

Signature validity is a complex topic, and this documentation cannot list all possible details.

Compression {#compression}
-----------

The interface as currently specified does not allow for control of compression.
Compressing and encrypting data that may contain both attacker-supplied material and sensitive material could leak information about the sensitive material (see the CRIME attack).

Unless an application knows for sure that no attacker-supplied material is present on the input, it should not compress during encryption.

Privacy Considerations
======================

Material produced by `sop encrypt` may be placed on untrusted machine (e.g., sent through the public `SMTP` network).
That material may contain metadata that leaks associational information (e.g., recipient identifiers in PKESK packets).
FIXME: document things like PURBs and `--hidden-recipient`)

Object Security vs. Transport Security
--------------------------------------

OpenPGP offers an object security model, but says little to nothing about how the secured objects get to the relevant parties.

When sending or receiving OpenPGP material, the implementer should consider what privacy leakage is implicit with the transport.


Document Considerations
=======================

\[ RFC Editor: please remove this section before publication ]

This document is currently edited as markdown.  Minor editorial
changes can be suggested via merge requests at
https://gitlab.com/dkg/openpgp-stateless-cli or by e-mail to the
authors.  Please direct all significant commentary to the public IETF
OpenPGP mailing list: openpgp@ietf.org

Document History
----------------


Future Work
-----------

 * `split` subcommand (split a clearsigned message into a message and a detached signature) (see {{detached-signatures}}
 * certificate transformation into popular publication forms:
   - WKD
   - DANE OPENPGPKEY
   - Autocrypt
 * `sop encrypt` -- specify compression? (see {{compression}})
 * `sop encrypt` -- specify padding policy/mechanism?
 * `sop decrypt` -- how can it more safely handle zip bombs?
 * `sop decrypt` -- what should it do when encountering weakly-encrypted (or unencrypted) input?
 * `sop encrypt` -- minimize metadata (e.g. `--throw-keyids`)?
 * handling secret keys that are locked with passwords?
 * do we need an interface (for performance?) that says "don't validate certificates internally, just accept whatever's given as legit data"? (see {{cert-validity-performance}})

Acknowledgements
================

This work was inspired by Justus Winter's {{OpenPGP-Interoperability-Test-Suite}}, and discussions with Justus.
Problems with this spec are not his fault.
